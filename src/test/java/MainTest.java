import org.example.*;
import org.junit.jupiter.api.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class MainTest {

    private static List<SearchResultsPage.ArticleDetails> articlesDetails;

    @BeforeAll
    public static void setUp() {
        articlesDetails = getFirstFourArticlesDetails();
    }

    public static List<SearchResultsPage.ArticleDetails> getFirstFourArticlesDetails() {

        WebDriver driver = new ChromeDriver();

        // Maximize the browser window
        driver.manage().window().maximize();

        // Navigate to the home page
        driver.get("https://link.springer.com/");

        // Create instances of HomePage, SignupLoginPage, and AdvancedSearchPage
        HomePage homePage = new HomePage(driver);
        SignupLoginPage signupLoginPage = new SignupLoginPage(driver);
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);

        // Navigate to the login/signup page
        homePage.login();

        // Log in a user with username and password
        signupLoginPage.login("novotd24@fel.cvut.cz", "aaaaaaaaaaaa");

        // Navigate to the advanced search page
        driver.get("https://link.springer.com/advanced-search");

        // Fill in the advanced search form
        // For year specification, select the option "in" and then enter "2024"
        advancedSearchPage.fillAdvancedSearchForm("Page Object Model", "Selenium Testing", "in", "2024");

        // Submit the advanced search form
        advancedSearchPage.submitAdvancedSearchForm();

        // Get the search results page
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);

        // Get and save the details of the first four articles
        List<SearchResultsPage.ArticleDetails> articlesDetails = searchResultsPage.getFirstFourArticlesDetails();

        // Output the details of the first four articles
        for (SearchResultsPage.ArticleDetails articleDetails : articlesDetails) {
            System.out.println("Name: " + articleDetails.getName());
            System.out.println("DOI: " + articleDetails.getDoi());
            System.out.println("Publication Date: " + articleDetails.getPublicationDate());
            System.out.println();
        }

        // Close the browser
        driver.quit();
        return articlesDetails;
    }

    @ParameterizedTest
    @MethodSource("getArticlesData")
    public void testArticleSearch(String articleName, String expectedDoi, String expectedPublicationDate) {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://link.springer.com/");
        WebElement element = driver.findElement(By.xpath("/html/body/dialog/div[2]/div/div[2]/button[2]"));
        element.click();

        // Find the search input field and enter the article name
        WebElement searchInput = driver.findElement(By.id("homepage-search"));
        searchInput.sendKeys(articleName);
        searchInput.submit();

        // Wait for the search results to load
        // (You may need to implement a wait mechanism here)

        // Get the first article in the search results
        WebElement firstArticle = driver.findElement(By.xpath("//li[@class='app-card-open app-card-open--has-image'][1]"));

        // Get the DOI and name of the first article
        String doi = firstArticle.findElement(By.tagName("a")).getAttribute("href");
        String name = firstArticle.findElement(By.className("app-card-open__heading")).getText();

        // Click on the first article to view its details
        firstArticle.findElement(By.tagName("a")).click();

        // Get the publication date of the article
        ArticlePage articlePage = new ArticlePage(driver);
        String publicationDate = articlePage.getPublicationDate();

        // Close the browser
        driver.quit();

        // Assertions
        Assertions.assertEquals(expectedDoi, doi);
        Assertions.assertEquals(expectedPublicationDate, publicationDate);
    }

    // Method to provide article names as test data
    public static List<Object[]> getArticlesData() {
        List<Object[]> articlesData = new ArrayList<>();
        for (SearchResultsPage.ArticleDetails article : articlesDetails) {
            String name = article.getName();
            String doi = article.getDoi();
            String publicationDate = article.getPublicationDate();
            articlesData.add(new Object[]{name, doi, publicationDate});
        }
        return articlesData;
    }
}
