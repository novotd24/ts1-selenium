package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ArticlePage {
    private WebDriver driver;

    @FindBy(css = "#main > section > div > div > div.app-article-masthead__info > ul:nth-child(3) > li > time")
    private WebElement publicationDateElement;

    // Constructor to initialize WebDriver and WebElements
    public ArticlePage(WebDriver driver) {
        this.driver = driver;
        // Initialize WebElements using PageFactory
        PageFactory.initElements(driver, this);
    }

    // Method to get the publication date of the article
    public String getPublicationDate() {
        return publicationDateElement.getText();
    }
}

