package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        WebDriver driver = new ChromeDriver();

        // Maximize the browser window
        driver.manage().window().maximize();

        // Navigate to the home page
        driver.get("https://link.springer.com/");

        // Create instances of HomePage, SignupLoginPage, and AdvancedSearchPage
        HomePage homePage = new HomePage(driver);
        SignupLoginPage signupLoginPage = new SignupLoginPage(driver);
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage(driver);

        // Navigate to the login/signup page
        homePage.login();

        // Log in a user with username and password
        signupLoginPage.login("novotd24@fel.cvut.cz", "aaaaaaaaaaaa");

        // Navigate to the advanced search page
        driver.get("https://link.springer.com/advanced-search");

        // Fill in the advanced search form
        // For year specification, select the option "in" and then enter "2024"
        advancedSearchPage.fillAdvancedSearchForm("Page Object Model", "Selenium Testing", "in", "2024");

        // Submit the advanced search form
        advancedSearchPage.submitAdvancedSearchForm();

        // Get the search results page
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);

        // Get and save the details of the first four articles
        List<SearchResultsPage.ArticleDetails> articlesDetails = searchResultsPage.getFirstFourArticlesDetails();

        // Output the details of the first four articles
        for (SearchResultsPage.ArticleDetails articleDetails : articlesDetails) {
            System.out.println("Name: " + articleDetails.getName());
            System.out.println("DOI: " + articleDetails.getDoi());
            System.out.println("Publication Date: " + articleDetails.getPublicationDate());
            System.out.println();
        }

        // Close the browser
        driver.quit();
    }
}