package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AdvancedSearchPage {
    private WebDriver driver;

    // Declare WebElements for advanced search page
    @FindBy(id = "all-words")
    private WebElement allWordsInput;

    @FindBy(id = "least-words")
    private WebElement leastWordsInput;

    @FindBy(id = "date-facet-mode")
    private WebElement dateFacetModeDropdown;

    @FindBy(id = "facet-start-year")
    private WebElement startYearInput;

    @FindBy(id = "submit-advanced-search")
    private WebElement submitButton;

    // Constructor to initialize WebDriver and WebElements
    public AdvancedSearchPage(WebDriver driver) {
        this.driver = driver;
        // Initialize WebElements using PageFactory
        PageFactory.initElements(driver, this);
    }

    // Method to fill in the advanced search form
    public void fillAdvancedSearchForm(String allWords, String leastWords, String dateFacetMode, String startYear) {
        allWordsInput.sendKeys(allWords);
        leastWordsInput.sendKeys(leastWords);

        // Select the date facet mode from dropdown
        Select dateFacetModeSelect = new Select(dateFacetModeDropdown);
        dateFacetModeSelect.selectByValue(dateFacetMode);

        // Enter the start year
        startYearInput.sendKeys(startYear);
    }

    // Method to submit the advanced search form
    public void submitAdvancedSearchForm() {
        submitButton.click();
    }
}
