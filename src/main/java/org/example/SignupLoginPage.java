package org.example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignupLoginPage {
    private WebDriver driver;

    // Declare WebElements for the login/signup page
    @FindBy(id = "login-email")
    private WebElement emailInput;

    @FindBy(id = "email-submit")
    private WebElement emailSubmitButton;

    @FindBy(id = "login-password")
    private WebElement passwordInput;

    @FindBy(id = "password-submit")
    private WebElement passwordSubmitButton;

    // Constructor to initialize WebDriver and WebElements
    public SignupLoginPage(WebDriver driver) {
        this.driver = driver;
        // Initialize WebElements using PageFactory
        PageFactory.initElements(driver, this);
    }

    // Method to log in a user
    public void login(String username, String password) {
        // Enter username and click continue
        emailInput.sendKeys(username);
        emailSubmitButton.click();

        // Enter password and click continue
        passwordInput.sendKeys(password);
        passwordSubmitButton.click();
    }
}
