package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private WebDriver driver;

    // Declare WebElement for the login button
    @FindBy(id = "identity-account-widget")
    private WebElement loginButton;

    // Constructor to initialize WebDriver and WebElements
    public HomePage(WebDriver driver) {
        this.driver = driver;
        // Initialize WebElements using PageFactory
        PageFactory.initElements(driver, this);
    }

    // Method to click on the login button and navigate to login/signup page
    public void goToLoginSignupPage() {
        loginButton.click();
    }

    // Method to click on the specified element before navigating to login/signup page
    public void login() {
        // Click on the specified element
        WebElement element = driver.findElement(By.xpath("/html/body/dialog/div[2]/div/div[2]/button[2]"));
        element.click();

        // Navigate to login/signup page
        goToLoginSignupPage();
    }
}
