package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsPage {
    private WebDriver driver;

    // Constructor to initialize WebDriver
    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        // Initialize WebElements using PageFactory
        PageFactory.initElements(driver, this);
    }

    public List<ArticleDetails> getFirstFourArticlesDetails() {
        List<ArticleDetails> articles = new ArrayList<>();
        List<WebElement> articleElements = driver.findElements(By.xpath("//li[@class='app-card-open app-card-open--has-image']"));

        for (int i = 0; i < Math.min(4, articleElements.size()); i++) {
            // Re-fetch the article elements on each iteration to avoid stale element reference
            articleElements = driver.findElements(By.xpath("//li[@class='app-card-open app-card-open--has-image']"));
            WebElement articleElement = articleElements.get(i);

            // Get name and DOI for each article
            String name = articleElement.findElement(By.className("app-card-open__heading")).getText();
            String doi = articleElement.findElement(By.tagName("a")).getAttribute("href");

            // Click on the article title to navigate to the article page
            articleElement.findElement(By.tagName("a")).click();

            // Create an ArticlePage object to retrieve the publication date
            ArticlePage articlePage = new ArticlePage(driver);

            // Get the publication date from the article page
            String publicationDate = articlePage.getPublicationDate();

            // Add the details to the list
            ArticleDetails articleDetails = new ArticleDetails(name, doi, publicationDate);
            articles.add(articleDetails);

            // Go back to the search results page
            driver.navigate().back();
        }

        return articles;
    }


    // Inner class to represent the details of an article
    public static class ArticleDetails {
        private String name;
        private String doi;
        private String publicationDate;

        public ArticleDetails(String name, String doi, String publicationDate) {
            this.name = name;
            this.doi = doi;
            this.publicationDate = publicationDate;
        }

        public String getName() {
            return name;
        }

        public String getDoi() {
            return doi;
        }

        public String getPublicationDate() {
            return publicationDate;
        }
    }
}

